(* Finite maps based on 2-3 trees *)

signature NotFound (Key V : Type) =
| not_found : Key => V

type Order = sig
  type this : Type
  val compare : this -> this -> Ord
end

module this (Key : Order) = struct
  data rec Tree (V : Type) =
  | Leaf
  | Node2 of Tree V, Key, V, Tree V
  | Node3 of Tree V, Key, V, Tree V, Key, V, Tree V

  data this (V : Type) =
    { size : Int
    ; tree : Tree V
    }

  data ATree (V : Type) =
  | ADone  of Tree V
  | ANode2 of Tree V, Key, V, Tree V
  
  data RTree (V : Type) =
  | RDone  of Tree V
  | RRem   of Tree V (* depth of tree was decreased *)
  
  data LPart (V : Type) =
  | LPart2 of Tree V, Key, V
  | LPart3 of Tree V, Key, V, Tree V, Key, V
  
  data RPart (V : Type) =
  | RPart2 of Key, V, Tree V
  | RPart3 of Key, V, Tree V, Key, V, Tree V

  signature TreeSize =
  | inc_size : Unit => Unit
  | dec_size : Unit => Unit

  let withSize m =
    handler
    | return tree => fn size => { size = size; tree = tree }
    | inc_size () => fn size => resume () (size + 1)
    | dec_size () => fn size => resume () (size - 1)
    | finally f   => f m.size
    end

  let ofATree t =
    match t with
    | ADone  t       => t
    | ANode2 l x v r => Node2 l x v r
    end
 
  let ofRTree t =
    match t with
    | RDone t => t
    | RRem  t => t
    end

  let aNode2_AT l k v r =
    match l with
    | ADone  l           => Node2 l k v r
    | ANode2 ll lk lv lr => Node3 ll lk lv lr k v r
    end

  let aNode2_TA l k v r =
    match r with
    | ADone  r           => Node2 l k v r
    | ANode2 rl rk rv rr => Node3 l k v rl rk rv rr
    end

  let aNode3_ATT l k1 v1 m k2 v2 r =
    match l with
    | ADone  l           => ADone  (Node3 l k1 v1 m k2 v2 r)
    | ANode2 ll lk lv lr => ANode2 (Node2 ll lk lv lr) k1 v1 (Node2 m k2 v2 r)
    end

  let aNode3_TAT l k1 v1 m k2 v2 r =
    match m with
    | ADone  m           => ADone  (Node3 l k1 v1 m k2 v2 r)
    | ANode2 ml mk mv mr => ANode2 (Node2 l k1 v1 ml) mk mv (Node2 mr k2 v2 r)
    end

  let aNode3_TTA l k1 v1 m k2 v2 r =
    match r with
    | ADone  r           => ADone  (Node3 l k1 v1 m k2 v2 r)
    | ANode2 rl rk rv rr => ANode2 (Node2 l k1 v1 m) k2 v2 (Node2 rl rk rv rr)
    end

  let rNode2_RT l k v r =
    match l with
    | RDone l => RDone (Node2 l k v r)
    | RRem  l =>
      match r with
      | Leaf              => RRem (Node2 l k v Leaf)  (* Impossible case *)
      | Node2 rl rk rv rr => RRem (Node3 l k v rl rk rv rr)
      | Node3 rl rk1 rv1 rm rk2 rv2 rr =>
        RDone (Node2 (Node2 l k v rl) rk1 rv1 (Node2 rm rk2 rv2 rr))
      end
    end

  let rNode2_TR l k v r =
    match r with
    | RDone r => RDone (Node2 l k v r)
    | RRem  r =>
      match l with
      | Leaf              => RRem (Node2 Leaf k v r)  (* Impossible case *)
      | Node2 ll lk lv lr => RRem (Node3 l lk lv lr k v r)
      | Node3 ll lk1 lv1 lm lk2 lv2 lr =>
        RDone (Node2 (Node2 ll lk1 lv1 lm) lk2 lv2 (Node2 lr k v r))
      end
    end

  let rNode3_RTT l k1 v1 m k2 v2 r =
    match l with
    | RDone l => RDone (Node3 l k1 v1 m k2 v2 r)
    | RRem  l =>
      match m with
      | Leaf => RRem (Node3 l k1 v1 Leaf k2 v2 r) (* Impossible case *)
      | Node2 ml mk mv mr =>
        RDone (Node2 (Node3 l k1 v1 ml mk mv mr) k2 v2 r)
      | Node3 ml mk1 mv1 mm mk2 mv2 mr =>
        RDone (Node3 (Node2 l k1 v1 ml) mk1 mv1 (Node2 mm mk2 mv2 mr) k2 v2 r)
      end
    end

  let rNode3_TRT l k1 v1 m k2 v2 r =
    match m with
    | RDone m => RDone (Node3 l k1 v1 m k2 v2 r)
    | RRem  m =>
      match l with
      | Leaf => RRem (Node3 Leaf k1 v1 m k2 v2 r) (* Impossible case *)
      | Node2 ll lk lv lr =>
        RDone (Node2 (Node3 ll lk lv lr k1 v1 m) k2 v2 r)
      | Node3 ll lk1 lv1 lm lk2 lv2 lr =>
        RDone (Node3 (Node2 ll lk1 lv1 lm) lk2 lv2 (Node2 lr k1 v1 m) k2 v2 r)
      end
    end

  let rNode3_TTR l k1 v1 m k2 v2 r =
    match r with
    | RDone r => RDone (Node3 l k1 v1 m k2 v2 r)
    | RRem  r =>
      match m with
      | Leaf => RRem (Node3 l k1 v1 Leaf k2 v2 r) (* Impossible case *)
      | Node2 ml mk mv mr =>
        RDone (Node2 l k1 v1 (Node3 ml mk mv mr k2 v2 r))
      | Node3 ml mk1 mv1 mm mk2 mv2 mr =>
        RDone (Node3 l k1 v1 (Node2 ml mk1 mv1 mm) mk2 mv2 (Node2 mr k2 v2 r))
      end
    end

  let remA t =
    match t with
    | ADone  t       => RRem t
    | ANode2 l k v r => RDone (Node2 l k v r)
    end

  let rightSubtree t =
    match t with
    | Leaf          => None
    | Node2 l k v r => Some (LPart2 l k v, r)
    | Node3 l k1 v1 m k2 v2 r =>
      Some (LPart3 l k1 v1 m k2 v2, r)
    end

  let leftSubtree t =
    match t with
    | Leaf          => None
    | Node2 l k v r => Some (l, RPart2 k v r)
    | Node3 l k1 v1 m k2 v2 r =>
      Some (l, RPart3 k1 v1 m k2 v2 r)
    end

  let fillRPart l rp =
    match rp with
    | RPart2 k v r           => Node2 l k v r
    | RPart3 k1 v1 m k2 v2 r => Node3 l k1 v1 m k2 v2 r
    end

  let fillLPart lp r =
    match lp with
    | LPart2 l k v           => Node2 l k v r
    | LPart3 l k1 v1 m k2 v2 => Node3 l k1 v1 m k2 v2 r
    end

  let joinParts lp m rp =
    match lp with
    | LPart2 l k1 v1 =>
      match rp with
      | RPart2 k2 v2 r => ADone (Node3 l k1 v1 m k2 v2 r)
      | RPart3 k2 v2 rm k3 v3 r =>
        ANode2 (Node2 l k1 v1 m) k2 v2 (Node2 rm k3 v3 r)
      end
    | LPart3 l k1 v1 lm k2 v2 =>
      ANode2 (Node2 l k1 v1 lm) k2 v2 (fillRPart m rp)
    end

  let treeAdd k v t =
    let rec add t =
      match t with
      | Leaf =>
        inc_size ();
        ANode2 Leaf k v Leaf
      | Node2 t1 k1 v1 t2 =>
        match Key.compare k k1 with
        | LT => ADone (aNode2_AT (add t1) k1 v1 t2)
        | EQ => ADone (Node2 t1 k v t2)
        | GT => ADone (aNode2_TA t1 k1 v1 (add t2))
        end
      | Node3 t1 k1 v1 t2 k2 v2 t3 =>
        match Key.compare k k1 with
        | LT => aNode3_ATT (add t1) k1 v1 t2 k2 v2 t3
        | EQ => ADone (Node3 t1 k v t2 k2 v2 t3)
        | GT =>
          match Key.compare k k2 with
          | LT => aNode3_TAT t1 k1 v1 (add t2) k2 v2 t3
          | EQ => ADone (Node3 t1 k1 v1 t2 k v t3)
          | GT => aNode3_TTA t1 k1 v1 t2 k2 v2 (add t3)
          end
        end
      end
    in add t

  let rec treeMerge l r =
    match rightSubtree l with
    | None => ADone r
    | Some (p1, ls) =>
      match leftSubtree r with
      | None => ADone l
      | Some (rs, p2) =>
        match treeMerge ls rs with
        | ADone m            => joinParts p1 m p2
        | ANode2 ml mk mv mr =>
          ANode2 (fillLPart p1 ml) mk mv (fillRPart mr p2)
        end
      end
    end

  let treeRemove k t =
    let rec remove t =
      match t with
      | Leaf              => RDone Leaf
      | Node2 t1 k1 v1 t2 =>
        match Key.compare k k1 with
        | LT => rNode2_RT (remove t1) k1 v1 t2
        | EQ =>
          dec_size ();
          remA (treeMerge t1 t2)
        | GT => rNode2_TR t1 k1 v1 (remove t2)
        end
      | Node3 t1 k1 v1 t2 k2 v2 t3 =>
        match Key.compare k k1 with
        | LT => rNode3_RTT (remove t1) k1 v1 t2 k2 v2 t3
        | EQ =>
          dec_size ();
          RDone (aNode2_AT (treeMerge t1 t2) k2 v2 t3)
        | GT =>
          match Key.compare k k2 with
          | LT => rNode3_TRT t1 k1 v1 (remove t2) k2 v2 t3
          | EQ =>
            dec_size ();
            RDone (aNode2_TA t1 k1 v1 (treeMerge t2 t3))
          | GT => rNode3_TTR t1 k1 v1 t2 k2 v2 (remove t3)
          end
        end
      end
    in remove t

  let find k m =
    let rec find t =
      match t with
      | Leaf => not_found k
      | Node2 t1 k1 v1 t2 =>
        match Key.compare k k1 with
        | LT => find t1
        | EQ => v1
        | GT => find t2
        end
      | Node3 t1 k1 v1 t2 k2 v2 t3 =>
        match Key.compare k k1 with
        | LT => find t1
        | EQ => v1
        | GT =>
          match Key.compare k k2 with
          | LT => find t2
          | EQ => v2
          | GT => find t3
          end
        end
      end
    in find m.tree

  let mem k m =
    let rec mem t =
      match t with
      | Leaf => False
      | Node2 t1 k1 _ t2 =>
        match Key.compare k k1 with
        | LT => mem t1
        | EQ => True
        | GT => mem t2
        end
      | Node3 t1 k1 _ t2 k2 _ t3 =>
        match Key.compare k k1 with
        | LT => mem t1
        | EQ => True
        | GT =>
          match Key.compare k k2 with
          | LT => mem t2
          | EQ => True
          | GT => mem t3
          end
        end
      end
    in mem m.tree

  let rec treeIter f m =
    match m with
    | Leaf => ()
    | Node2 t1 k1 v1 t2 =>
      treeIter f t1;
      f k1 v1;
      treeIter f t2
    | Node3 t1 k1 v1 t2 k2 v2 t3 =>
      treeIter f t1;
      f k1 v1;
      treeIter f t2;
      f k2 v2;
      treeIter f t3
    end

  let empty {V : Type} =
    { size = 0
    ; tree = Leaf
    }

  let singleton k v =
    { size = 0
    ; tree = Node2 Leaf k v Leaf
    }

  let add k v m =
    handle ofATree (treeAdd k v m.tree) with withSize m

  let remove k m =
    handle ofRTree (treeRemove k m.tree) with withSize m

  let iter f m = treeIter f m.tree

  let size m = m.size
end
