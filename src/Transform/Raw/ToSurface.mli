
val tr_program   : Lang.Raw.file      -> Lang.Surface.source_file
val tr_interface : Lang.Raw.intf_file -> Lang.Surface.intf_file

val source_flow_tag : Flow.tag
val intf_flow_tag   : Flow.tag
