open Common

let this_index decls =
  let rec this_index_aux decls idx =
    match decls with
    | [] -> raise Not_found
    | decl :: decls ->
      begin match decl with
      | S.DeclThis _ -> idx
      | S.DeclTypedef _ | S.DeclVal _ | S.DeclOp _ | S.DeclCtor _
      | S.DeclField _ ->
        this_index_aux decls (idx+1)
      end
  in
  this_index_aux decls 0

let typedef_index decls =
  let rec typedef_index_aux decls idx =
    match decls with
    | [] -> raise Not_found
    | decl :: decls ->
      begin match decl with
      | S.DeclTypedef _ -> idx
      | S.DeclThis _ | S.DeclVal _ | S.DeclOp _ | S.DeclCtor _
      | S.DeclField _ ->
        typedef_index_aux decls (idx+1)
      end
  in
  typedef_index_aux decls 0

let val_index decls name =
  let rec val_index_aux decls idx =
    match decls with
    | [] -> raise Not_found
    | decl :: decls ->
      begin match decl with
      | S.DeclVal(name', _) when name' = name -> idx
      | S.DeclThis _ | S.DeclTypedef _ | S.DeclVal _ | S.DeclOp _
      | S.DeclCtor _ | S.DeclField _ ->
        val_index_aux decls (idx+1)
      end
  in
  val_index_aux decls 0

let op_index decls name =
  let rec op_index_aux decls idx =
    match decls with
    | [] -> raise Not_found
    | decl :: decls ->
      begin match decl with
      | S.DeclOp(_, _, ops, n) when
          (S.OpDecl.name' ops n = name) -> idx
      | S.DeclThis _ | S.DeclTypedef _ | S.DeclVal _ | S.DeclOp _
      | S.DeclCtor _ | S.DeclField _ ->
        op_index_aux decls (idx+1)
      end
  in
  op_index_aux decls 0

let ctor_index decls name =
  let rec ctor_index_aux decls idx =
    match decls with
    | [] -> raise Not_found
    | decl :: decls ->
      begin match decl with
      | S.DeclCtor(_, _, ctors, n) when
          (S.CtorDecl.name' ctors n = name) -> idx
      | S.DeclThis _ | S.DeclTypedef _ | S.DeclVal _ | S.DeclOp _
      | S.DeclCtor _ | S.DeclField _ ->
        ctor_index_aux decls (idx+1)
      end
  in
  ctor_index_aux decls 0

let field_index decls name =
  let rec field_index_aux decls idx =
    match decls with
    | [] -> raise Not_found
    | decl :: decls ->
      begin match decl with
      | S.DeclField(_, _, flds, n) when
          (S.FieldDecl.name' flds n = name) -> idx
      | S.DeclThis _ | S.DeclTypedef _ | S.DeclVal _ | S.DeclOp _
      | S.DeclCtor _ | S.DeclField _ ->
        field_index_aux decls (idx+1)
      end
  in
  field_index_aux decls 0
