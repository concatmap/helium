open Common

val this_index    : S.decl list -> int
val typedef_index : S.decl list -> int
val val_index     : S.decl list -> string -> int
val op_index      : S.decl list -> string -> int
val ctor_index    : S.decl list -> string -> int
val field_index   : S.decl list -> string -> int
