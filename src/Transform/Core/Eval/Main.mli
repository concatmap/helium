type value

val run : Lang.Core.source_file -> value

val flow_node : value Flow.node

val eval_tag : Flow.tag
