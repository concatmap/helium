
val register_extern : string -> Value.t -> unit
val extern_exists   : string -> bool
val get_extern      : string -> Value.t
