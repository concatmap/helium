open Kind
include Kind.Export

module TVar    : Common.TVar_S
module EffInst : Common.EffInst_S
module Var     : Common.Var_S

type 'k tvar = 'k Common.tvar
type effinst = Common.effinst

type 'k typ

type ttype  = ktype typ
type effect = keffect typ
type effsig = keffsig typ

type subst
type scope

type type_arg =
| TpArg : 'k typ -> type_arg

type 'k type_future_id =
| TFIdType   : Utils.UID.t -> ktype   type_future_id
| TFIdEffect : Utils.UID.t -> keffect type_future_id
| TFIdEffsig : Utils.UID.t -> keffsig type_future_id

type 'k type_future =
| TFType   of 'k typ
| TFFuture of 'k type_future_id * type_arg list * effinst list

type 'k neutral_type =
| TVar of 'k tvar
| TApp : ('k1 -> 'k2) neutral_type * 'k1 typ -> 'k2 neutral_type

type ctor_decl =
| CtorDecl of string * TVar.ex list * ttype list

type field_decl =
| FieldDecl of string * ttype

type op_decl =
| OpDecl of string * TVar.ex list * ttype list * ttype

type 'k type_view =
| TEffPure    : keffect type_view
| TEffCons    : effect * effect -> keffect type_view
| TEffInst    : effinst -> keffect type_view
| TBase       : RichBaseType.t -> ktype type_view
| TNeutral    : 'k neutral_type -> 'k type_view
| TTuple      : ttype list -> ktype type_view
| TArrow      : ttype * ttype * effect -> ktype type_view
| TForall     : 'k tvar * ttype -> ktype type_view
| TExists     : 'k tvar * ttype -> ktype type_view
| TForallInst : effinst * effsig * ttype -> ktype type_view
| TDataDef    : ttype * ctor_decl list -> ktype type_view
| TRecordDef  : ttype * field_decl list -> ktype type_view
| TEffsigDef  : effsig * op_decl list -> ktype type_view
| TFun        : 'k1 tvar * 'k2 typ -> ('k1 -> 'k2) type_view
| TFuture     :
  { uid    : Utils.UID.t
  ; tholes : type_arg list 
  ; eholes : effinst list 
  ; future : (unit -> 'k type_future)
  ; subst  : subst
  } -> 'k type_view

type var = Common.var

type typedef =
| TypeDef : 'k tvar * var * ttype -> typedef

type expr = (Utils.UID.t, expr_data) Node.node
and expr_data =
| EValue   of value
| ELet     of var * expr * expr
| ELetPure of var * expr * expr
| EFix     of rec_function list * expr
| EUnpack  :  'k tvar * var * value * expr -> expr_data
| ETypeDef of typedef list * expr
| ETypeApp :  value * 'k typ -> expr_data
| EInstApp of value * effinst
| EApp     of value * value
| EProj    of value * int
| ESelect  of value * int * value
| EMatch   of value * value * match_clause list * ttype
| EHandle  of
    { proof       : value
    ; effinst     : effinst
    ; body        : expr
    ; op_handlers : op_handler list
    ; return_var  : var
    ; return_body : expr
    ; htype       : ttype 
    ; heffect     : effect
    }
| EOp         of value * int * effinst * type_arg list * value list
| ERepl       of (unit -> expr CommonRepl.result) * ttype * effect * Box.t
| EReplExpr   of expr * Box.t * expr
| EReplImport of import list * expr

and rec_function =
| RFFun     of var * ttype * TVar.ex list * var * ttype * expr
| RFInstFun of var * ttype * TVar.ex list * effinst * effsig * expr

and match_clause =
| Clause of TVar.ex list * var list * expr

and op_handler =
| OpHandler of TVar.ex list * var list * var * expr

and value = (Utils.UID.t, value_data) Node.node
and value_data =
| VLit     of RichBaseType.lit
| VVar     of var
| VFn      of var * ttype * expr
| VTypeFun : 'k tvar * expr -> value_data
| VInstFun of effinst * effsig * expr
| VPack    : 'k typ * value * 'k tvar * ttype -> value_data
| VTuple   of value list
| VCtor    of value * int * type_arg list * value list
| VRecord  of value * value list
| VExtern  of string * ttype

and import =
  { im_name   : string
  ; im_path   : string
  ; im_level  : Common.source_level
  ; im_var    : var
  ; im_types  : TVar.ex list
  ; im_handle : effect
  ; im_sig    : ttype list
  }

type unit_body =
| UB_Direct of
  { types    : TVar.ex list
  ; body_sig : ttype list
  ; body     : expr
  }
| UB_CPS of
  { ambient_eff : effect
  ; types       : TVar.ex list
  ; handle      : effect
  ; body_sig    : ttype list
  ; body        : expr
  }

type source_file =
  { sf_import : import list
  ; sf_body   : unit_body
  }

module Type : sig
  exception Escapes_scope

  val base        : RichBaseType.t -> ttype
  val var         : 'k tvar -> 'k typ
  val neutral     : 'k neutral_type -> 'k typ
  val tuple       : ttype list -> ttype
  val arrow       : ttype -> ttype -> effect -> ttype
  val forall      : 'k tvar -> ttype -> ttype
  val exists      : 'k tvar -> ttype -> ttype
  val forall_inst : effinst -> effsig -> ttype -> ttype

  val eff_pure : effect
  val eff_cons : effect -> effect -> effect
  val eff_inst : effinst -> effect

  val data_def   : ttype -> ctor_decl list -> ttype
  val record_def : ttype -> field_decl list -> ttype
  val effsig_def : effsig -> op_decl list -> ttype

  val forall_l : TVar.ex list -> ttype -> ttype
  val exists_l : TVar.ex list -> ttype -> ttype

  val tfun : 'k1 tvar -> 'k2 typ -> ('k1 -> 'k2) typ

  val future : (unit -> 'k type_future) -> subst -> 'k typ

  val kind : 'k typ -> 'k kind
  val view : 'k typ -> 'k type_view

  val neutral_kind : 'k neutral_type -> 'k kind

  val subst      : subst -> 'k typ -> 'k typ
  val subst_type : 'k1 tvar -> 'k1 typ -> 'k2 typ -> 'k2 typ
  val subst_inst : effinst -> effinst -> 'k typ -> 'k typ

  val equal     : 'k typ -> 'k typ -> bool
  val subeffect : effect -> effect -> bool
  val subtype   : ttype  -> ttype  -> bool

  val check_scope        : scope -> 'k typ -> bool
  val supertype_in_scope : scope -> ttype -> ttype

  val to_sexpr : 'k typ -> SExpr.t

  (** types with existential kinds *)
  module Ex : Utils.Exists.S with type 'k data = 'k typ
  include module type of Ex.Datatypes
end

module Subst : sig
  val empty : subst

  val add_type : subst -> 'k tvar -> 'k typ -> subst
  val add_inst : subst -> effinst -> effinst -> subst

  val compose : subst -> subst -> subst
end

(** Operations on syntax *)
module Syntax : sig
  (** Apply type and instance substitution in an expression *)
  val apply_subst_in_expr : subst -> expr -> expr
end

module Scope : sig
  val empty       : scope
  val add_tvar    : scope -> 'k tvar -> scope
  val add_effinst : scope -> effinst -> scope
end

module SExprPrinter : sig
  val tr_value : value -> SExpr.t
end

val flow_node : source_file Flow.node
