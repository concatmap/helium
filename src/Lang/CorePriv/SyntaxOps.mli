(* Lang/CorePriv/SyntaxOps.mli
 *
 * This file is part of Helium, developed under MIT license.
 * See LICENSE for details.
 *)
open TypingStructure
open Syntax

(** Apply type and instance substitution in an expression *)
val apply_subst_in_expr : subst -> expr -> expr
