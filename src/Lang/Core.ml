include CorePriv.TypingStructure
include CorePriv.Syntax

type scope = CorePriv.Scope.t

module Type = struct
  include CorePriv.Type
  include CorePriv.Subtyping
  include CorePriv.ScopeCheck
  let to_sexpr = CorePriv.SExprPrinter.tr_type
end

module Subst = struct
  include CorePriv.Subst
  let compose = CorePriv.Type.subst_compose
end

module Syntax = CorePriv.SyntaxOps
module Scope = CorePriv.Scope
module SExprPrinter = CorePriv.SExprPrinter

let flow_node = Flow.Node.create
  ~cmd_line_flag: "-core"
  "Core"

let _ : Flow.tag =
  Flow.register_transform
    ~provides_tags: [ CommonTags.pretty ]
    ~source: flow_node
    ~target: SExpr.flow_node
    ~name:   "Core --> SExpr (pretty printer)"
    (fun file _ -> Flow.return (CorePriv.SExprPrinter.tr_source_file file))
